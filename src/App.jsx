import Header from "./components/Header";
import Main from "./pages/Main";

export default function App() {
  

  return (
    <div className="mx-auto box-border overflow-x-hidden">
      <Header />
      <Main />
    </div>
  );
}
